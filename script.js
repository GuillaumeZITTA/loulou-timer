
class Timer {
   
    /**
     * Creates an instance of Timer.
     * @param {HTMLElement} field
     * @param {HTMLElement} button
     * @param {HTMLElement} svg
     * @memberof Timer
     */
    constructor(field, button, svg) {
        this.field = field;
        this.button = button;
        this.svg = svg;

        var self = this;

        // Setup key
        this.field.addEventListener("keyup", function(event){
            event.preventDefault();
            if (event.keyCode === 13) {
                self.start();
            }
        });

        // Setup clic
        this.button.addEventListener("click", function(event){
            event.preventDefault();
            self.start();
        })

        this.field.focus();

        this.field.value = "";

        this.targetTime = 0;

        $.each( window.sounds, function( key, val ) {
            $("<option>")
                .attr("value",val)
                .html(key)
                .appendTo("#inputGroupSelectSound");
        });

    }

    polarToCartesian(centerX, centerY, radius, angleInDegrees) {
        var angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;
        
        return {
            x: centerX + (radius * Math.cos(angleInRadians)),
            y: centerY + (radius * Math.sin(angleInRadians))
        };
    }
    
    describeArc(x, y, radius, startAngle, endAngle){

        var start = this.polarToCartesian(x, y, radius, endAngle);
        var end = this.polarToCartesian(x, y, radius, startAngle);

        var arcSweep = endAngle - startAngle <= 180 ? "0" : "1";

        var d = [
            "M", start.x, start.y, 
            "A", radius, radius, 0, arcSweep, 0, end.x, end.y,
            "L", x,y,
            "L", start.x, start.y
        ].join(" ");

        return d;       
    }

    /**
     * generate a SVG path from a time value
     *
     * @param {number} time in ms
     * @returns 
     * @memberof Timer
     */
    pathDescFromTime(time) {
        var angle = time * (360 / 3600 / 1000);
        return this.describeArc(100, 100, 95, 0, angle);
    }

    formatTime(time) {
        var timeSeconds = Math.floor(time / 1000);
        return `${Math.floor(timeSeconds / 60)}:${timeSeconds % 60}`;
    }

    remainingTime() {
        return this.targetTime - Date.now()
    }

    start(){
        if (this.remainingTime > 0) {
            console.log("already started");
            return false;
        }

        this.targetTime = (parseInt(this.field.value * 60) % 3600) * 1000 + Date.now();

        this.svg.getElementById("arc2").setAttribute("d", this.pathDescFromTime(this.remainingTime()));
        this.animate();
    }

    animate() {
        //console.log(this);
        if (this.remainingTime() > 0) {
            this.svg.getElementById("arc1").setAttribute("d", this.pathDescFromTime(this.remainingTime()));
            this.svg.getElementById("textincircle").innerHTML = this.formatTime(this.remainingTime());
        
            window.setTimeout(this.animate.bind(this), 500);
        } else {
            this.targetTime = 0;
            this.end();
        }
    }

    end() {
        $('#endModalCenter').modal('show');
        var soundSelector = window.document.getElementById("inputGroupSelectSound");
        var soundfile=soundSelector.value;
        if (soundfile != "") {
            var sound = window.document.getElementById("sound");
            sound.setAttribute("src", soundfile);
            sound.load();
            sound.play();
            $('#endModalCenter').on('hidden.bs.modal', function () {
                sound.pause();
            });
        }
    }
}